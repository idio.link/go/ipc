/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package internal

import (
	"fmt"
	"io"
)

func Receive(c io.Reader, buf []byte) (b []byte, err error) {
	n, err := c.Read(buf)
	if err != nil {
		err = fmt.Errorf("receive: %w", err)
	}
	b = buf[:n]
	return
}

func Send(c io.Writer, buf []byte) error {
	n, err := c.Write(buf)
	if err != nil {
		return fmt.Errorf("send: %w", err)
	}
	if n < len(buf) {
		return fmt.Errorf("send: sent incomplete message: %v", buf[:n])
	}
	if n > len(buf) {
		return fmt.Errorf("send: sent message with excess bytes: %v", buf[:n])
	}
	return nil
}
