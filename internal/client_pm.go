/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package internal

func NewClientPM() *ClientPM {
	return &ClientPM{}
}

type ClientPMState int

const (
	ClientPMStateNihil ClientPMState = iota
)

/*
Protocol machine for the IPC Client protocol.

The CPM is a symmetric protocol used by IPC clients to
communicate with the local IPC service. This protocol
provides a means of requesting allocation of IPC resources
by the network for communication with a target application
process (AP), AP instance, application protocol machine
(APM), or APM instance, contingent on criteria such as proof
of prior registration (i.e. authentication) and whether the
requested communication is permitted by the target (i.e.
authorization).

While authorization is somewhat expensive in our model (as
we must allocate the requested resources in order to
determine whether the target even exists, let alone whether
it accepts the request), we can (and should) cache
unfavorable, and only unfavorable, results until such time
as the cache lifetime, as suggested by the target, expires.
And since we expect other network elements in other
administrative domains to abide by this lifetime, there is
little point in attempting to bypass said lifetime in our
own administrative domain.

The same observation also applies to requests made by
authorized clients, where the rate of successful requests
for resources exceeds some network-wide "decorum" threshold,
above which subsequent requests may be shaped, policed, or
simply dropped outright for a period of time by upstream
network elements. In the worst case, the target may choose
to begin propagating authorization failures, resulting in
precisely the same cached result situation as above. And, of
course, network operators may always elect to drop all
subsequent requests for all targets, indefinitely,
commensurate with prior activity, until such time as the
individual case can be reviewed and the equipment owner,
contacted.
*/
type ClientPM struct {
	state ClientPMState
}

/*
Notify PM of an event, given by the provided buffer.

Events are internal to the distributed IPC facility (DIF).
Compare with the notion of "East-West" traffic, which is
disanalogous insofar as our model allows for multiple
discontiguous layers of equal rank yet differing scope.
*/
func (pm *ClientPM) Event(e []byte) ([]byte, error) {
	return e, nil
}

/*
Generate an allocation request SDU.

Requests/responses are external to the distributed IPC
facility (DIF) and occur within a single processing system
(i.e. locally).

Since requests/responses represent a transition from
internal to external (and vice versa), and since the
corresponding API forms a natural (indeed, the only)
boundary between these two domains, many security
considerations will naturally tend to concentrate here.
While it is not the responsibility of the protocol to
enforce constraints around these considerations, it must
create affordances for the ambient implementation to do so.
*/
func (pm *ClientPM) ReqAlloc(buf []byte) []byte {
	buf[0] = 1
	return buf[:8]
}
