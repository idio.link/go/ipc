#!/bin/bash
#

declare cpu_range=8-11

sudo cset shield --kthread=on --cpu $cpu_range &&
	sudo cset proc --set user --exec -- \
		chrt --fifo 1 ./bin/ipcd &&
	sudo taskset shield --reset

