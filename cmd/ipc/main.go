/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package main

import (
	"log"

	"idio.link/go/ipc"
)

func main() {
	c := ipc.NewClient()
	defer func() {
		if err := c.Close(); err != nil {
			log.Printf("failed to close ipc client: %v", err)
		}
	}()

	if err := c.Alloc(); err != nil {
		log.Fatal(err)
	}
}
