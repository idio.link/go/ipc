/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"idio.link/go/ipc/internal"
)

func main() {
	log := internal.NewLogger()
	ctx := context.WithValue(context.Background(), "log", log)
	ctx, cancel := context.WithCancel(ctx)

	log.Info("establish signal traps")
	go func() {
		c := make(chan os.Signal, 1)
		defer close(c)

		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		<-c
		cancel()
	}()

	log.Info("exiting: %v", err)
	os.Exit(1)
}
