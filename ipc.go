/*
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

package ipc

import (
	"fmt"
	"net"

	"idio.link/go/ipc/internal"
)

type LogLevel int

func NewClient() *Client {
	return &Client{
		cpm: internal.NewClientPM(),
		log: internal.NewLogger(),
	}
}

type Client struct {
	conn net.Conn
	cpm  *internal.ClientPM
	log  *internal.Logger
}

func (c *Client) LogLevel(l LogLevel) {
	c.log.Level(internal.LogLevel(l))
}

func (c *Client) Close() (err error) {
	err = c.conn.Close()
	if err != nil {
		err = fmt.Errorf("ipc client: failed to close connection: %v", err)
	}
	return
}

func (c *Client) Alloc() (err error) {
	c.log.Debug("connect to ipc service")

	abstract := "\000idio.link/go/ipc"
	c.conn, err = net.Dial("unixpacket", abstract)
	if err != nil {
		err = fmt.Errorf("ipc client alloc: %v", err)
		return
	}

	c.log.Debug("send allocation request")

	buf := make([]byte, 1024)
	sdu := c.cpm.ReqAlloc(buf)
	err = internal.Send(c.conn, sdu)
	if err != nil {
		_ = c.Close()
		err = fmt.Errorf("send: %v", err)
		return
	}
	sdu, err = internal.Receive(c.conn, buf)
	if err != nil {
		_ = c.Close()
		err = fmt.Errorf("receive: %v", err)
	}
	c.log.Debug("got %q\n", sdu)
	return
}
